
CONTENTS:

A) Disclaimer 
B) Structure of the code
C) How to run the code 

A) ------- disclaimer -------

This program is provide open-source, 
for free, as-is, and with no warranty. 

The authors waive any responsibility for any damage 
that may result from its use in any context.

Please cite as 
G. Casu, http://tiny.cc/qfyomz; 
G. Casu, A. Bosin, and V. Fiorentini, to be published 

(the latter citation will be updated upon publication of the paper.)

B) ------- structure --------

There are 3 subdirectories: scriptBTP2, input, dataBTP2.

*--- Contents of 'scriptBTP2':

CRT.py   ==> constant relaxation time thermoelectric coeffs

ART.py   ==> averaged relaxation time thermoelectric coeffs

NCRT.py ==> non-constant relaxation time thermoelectric coeffs

TAU.py   ==> calculation of tau(E,T) for external use and plots

plotter.py  ==> code doing the plotting

relaxtau.py ==> library containing the relaxation time model tau(E,T)

funzVar.py  ==> library with various auxiliary stuff

do.sh   :  sh script executing all three code flavors

d2u.sh  :  script converting CRLF=>LF line ending


*--- Contents of 'input':

vasprun.xml ==> the ab initio el. struc. calculation, must be provided by you.
		the one in the original program package is for the SNO paper.

input ==> input file with control parameters and materials parms data.
	  the one provided with the original program is for the SNO paper

latthcond ==> file with lattice thermal conductivity (optional).
	      the one provided with the original program is a placeholder.

*--- Contents of 'dataBTP2':

Data dump fetched by plotter.py. You won't need to look in here.


C) --------- how to install and run ----------

Given the minor disk occupation, the simplest way of using the code is unpacking and deploying it wherever it is needed.

Prerequisites: 
- python 3.6 with typical libraries is needed
- BoltzTrap2 must be installed for this code to work

troubleshoot:
- occasionally editors attach CR+LF line endings to files, which may 
  cause execution to fail. This can be avoided processing files with
  the script d2u.sh
  
Procedure:

0) unzip the whole package in your work dir (simple, negligible cost)

 In 'input' dir:

1) copy 'vasprun.xml'

2) prepare 'input' file

3) provide 'latthcond' file (optional, see 'input' file)

 In 'scriptBT2' dir:

4) run CRT.py and/or ART.py and/or NCRT.py, plus TAU.py

4b) alternatively run do.sh 

5) run plotter.py 

6) to do convergence tests, repeat with different inputs.
   the usual parameters should be tested, so at least grid, multiplier, 
   energy bins, energy range.

-------------------------------
